package tony.nz.plexure_challenge_android

import android.util.Log
import org.junit.Test

import org.junit.Assert.*
import tony.nz.plexure_challenge_android.uitis.Restaurant


class NetworkingHelperKtTest {
    private val TAG = "NetworkingHelperKtTest"
    @Test
    fun getDataAPI() {
        var data: List<Restaurant>? = null
        DataAPI.getData().subscribe(
            { retrievedData ->
                data = retrievedData
            },
            {err->
                Log.e(TAG, err.localizedMessage)
            },

            {
                assertNotNull(data)
                data?.let{
                    assertEquals(it.size, 3)
                }

            }
        )
    }
}