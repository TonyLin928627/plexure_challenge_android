package tony.nz.plexure_challenge_android

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import tony.nz.plexure_challenge_android.uitis.NetworkingHelper
import tony.nz.plexure_challenge_android.uitis.PreferencesHelper
import tony.nz.plexure_challenge_android.uitis.Restaurant


class MainActivity : AppCompatActivity(), IFavoriteRemovedListener {

    private var mFilter: IRestaurantFilterRule? = null
    override fun onFavoriteRemoved(position: Int) {
        //remote the un-favorited item from list view
        when(isFilteredForFavorites){
            true->{
                showFilteredData()
            }
        }
    }

    private val TAG = "MainActivity"
    private var mData: List<Restaurant> = emptyList()
    private val mAllFeaturesSet = mutableSetOf<String>()
    private var mSelectedFeatureSet: MutableSet<String>? = null
    private var mAdapter: RecyclerViewAdapter? = null
    private var mSortType  = SortType.NONE

    private var isFilteredForFavorites = false
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_all -> {
                mFilter = FeatureSelectedFilter(mSelectedFeatureSet!!)

                showFilteredData()
                when(this.mSortType){
                    SortType.ASSENDING -> {sortByDistance(R.id.assendingRb)}
                    SortType.DECIDING -> {sortByDistance(R.id.decidingRb)}
                }

                isFilteredForFavorites = false
                headerView.visibility = View.VISIBLE

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorited -> {
                mFilter = FavoriteRestaurantFilter()
                showFilteredData()
                isFilteredForFavorites = true
                headerView.visibility = View.GONE
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun showFilteredData(){

        //todo: execute the filter in background thread with Coroutines
        val filteredData = mData.filter {
            mFilter?.filter(it) ?: true
        }

        mAdapter?.setItems(filteredData)
        mAdapter?.notifyDataSetChanged()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferencesHelper.install(this)

        setContentView(R.layout.activity_main)
        initUI()
        loadData()
    }

    override fun onDestroy() {

        mLoadDataDisposable?.takeIf { !it.isDisposed }?.dispose()

        super.onDestroy()
    }

    private fun initUI(){
        //1. init Bottom tabs
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        //2. init list view
        mAdapter = RecyclerViewAdapter(this)
        mAdapter?.favoriteRemovedListener = this
        contentRv.layoutManager = LinearLayoutManager(this)
        contentRv.adapter = mAdapter

        //3. init pull to refresh
        pullToRefresh.setOnRefreshListener {
            loadData{
                if(!isFilteredForFavorites){
                    when(this.mSortType){
                        SortType.ASSENDING -> {sortByDistance(R.id.assendingRb)}
                        SortType.DECIDING -> {sortByDistance(R.id.decidingRb)}
                    }
                }
                pullToRefresh.isRefreshing = false

                true
            }
        }

        //4. init radio group for sorting type
        radioGroup.setOnCheckedChangeListener{ _,checkedId ->
            sortByDistance(checkedId)
        }
    }

    private fun sortByDistance(checkedId: Int){

        this.mSortType = when(checkedId){
            R.id.noneRb -> {
                showFilteredData()
                SortType.NONE
            }
            R.id.assendingRb -> {

                mAdapter?.getItems()?.let { restaurants ->
                    mAdapter?.setItems(restaurants.sortedBy {
                        it.distance
                    })

                    mAdapter?.notifyDataSetChanged()
                }

                SortType.ASSENDING
            }
            R.id.decidingRb -> {

                mAdapter?.getItems()?.let { restaurants ->
                    mAdapter?.setItems(restaurants.sortedByDescending {
                        it.distance
                    })

                    mAdapter?.notifyDataSetChanged()
                }

                SortType.DECIDING
            }
            else->{throw Throwable("Invalid Id")}
        }
    }

    var mLoadDataDisposable: Disposable? = null
    private fun loadData(completion: (()->Boolean)? = null){
        mLoadDataDisposable?.takeIf { !it.isDisposed }?.dispose()

        //load remote data with RxJava
        createLoadDataObservable().subscribeOn(Schedulers.io())
            .doOnSubscribe {
                showProgressDialog(this)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { data->

                    mData = data
                    for (item in mData){
                        mAllFeaturesSet.addAll(item.features)
                    }

                    if (mSelectedFeatureSet==null){
                        mSelectedFeatureSet = mutableSetOf()
                        mSelectedFeatureSet!!.addAll(mAllFeaturesSet)
                    }

                    mFilter = if (isFilteredForFavorites){
                        FavoriteRestaurantFilter()
                    }else{
                        FeatureSelectedFilter(mSelectedFeatureSet!!)
                    }
                    showFilteredData()

                    hideProgressDialog()

                    completion?.let { it() }
                },

                {err->
                    hideProgressDialog()
                    pullToRefresh.isRefreshing = false

                    AlertDialog.Builder(this)
                        .setTitle("Failed to load data")
                        .setMessage(err.localizedMessage)
                        .setPositiveButton("OKAY", null)
                        .show()

                    Log.e(TAG, err.localizedMessage)
                    mLoadDataDisposable?.takeIf { !it.isDisposed }?.dispose()
                }
            )
    }

    private fun createLoadDataObservable(): Observable<List<Restaurant>> {
        return Observable.create<List<Restaurant>> { emitter ->

            NetworkingHelper.loadData()?.let {
                emitter.onNext(it)
                emitter.onComplete()
            } ?: run {
                emitter.onError(Throwable("Failed to load data"))
            }
        }
    }

    private var mProgressDialog: ProgressDialog? = null
    private fun showProgressDialog(context: Context) {

        mProgressDialog?.dismiss()

        ProgressDialog(context).let {
            this.mProgressDialog = it
            it.setMessage("Loading data...")
            it.setCanceledOnTouchOutside(false)
            it.show()
        }

    }

    private fun hideProgressDialog() {
        mProgressDialog?.dismiss()
        mProgressDialog = null
    }

    fun onFeaturesFilterClicked(v: View){

        val displayNames = arrayOfNulls<String>(mAllFeaturesSet.size)
        val checkedItems = BooleanArray(mAllFeaturesSet.size)

        val allFeaturesList = mAllFeaturesSet.toList()
        for(i in 0 until displayNames.size){
            displayNames[i] = allFeaturesList[i]

            if(mSelectedFeatureSet!!.contains(displayNames[i])) {
                checkedItems[i] = true
            }
        }


        AlertDialog.Builder(this)
            .setTitle("Filter Features")
            .setMultiChoiceItems(displayNames, checkedItems) { _, which, isChecked ->
                checkedItems[which] = isChecked
            }

            .setPositiveButton("Okay") { dialog, _->
                for(i in 0 until checkedItems.size){
                    if (checkedItems[i]){
                        mSelectedFeatureSet!!.add(allFeaturesList[i])
                    }else{
                        mSelectedFeatureSet!!.remove(allFeaturesList[i])
                    }
                }

                mFilter = FeatureSelectedFilter(mSelectedFeatureSet!!)
                showFilteredData()
            }
            .setNegativeButton("Cancel", null)
            .show()
    }
}
