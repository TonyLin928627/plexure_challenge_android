package tony.nz.plexure_challenge_android

const val TOO_FAR_VAL = 80*1000

const val DATA_URL = "https://bitbucket.org/YahiaRagaePlex/plexure-challenge/raw/449a2452c03961d5d1a094af524148cc345523db/data.json"

enum class SortType {
    NONE,
    ASSENDING,
    DECIDING
}