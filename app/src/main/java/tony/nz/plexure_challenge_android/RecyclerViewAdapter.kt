package tony.nz.plexure_challenge_android

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import tony.nz.plexure_challenge_android.uitis.PreferencesHelper
import tony.nz.plexure_challenge_android.uitis.Restaurant

import java.util.*

class RecyclerViewAdapter(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "RecyclerViewAdapter"
    private val mItems: MutableList<Restaurant>
    private var parentView: View? = null
    var favoriteRemovedListener: IFavoriteRemovedListener? = null

    init {
        mItems = ArrayList()
    }

    fun setItems(data: List<Restaurant>) {
        this.mItems.clear()
        this.mItems.addAll(data)
    }

    fun getItems(): List<Restaurant>{
        return this.mItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        parentView = parent
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_view, parent, false)
        return RecyclerViewHolder(view)

    }

    override fun getItemId(position: Int): Long {
        return mItems[position].id
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (holder is RecyclerViewHolder) {

            val restaurant = mItems[position]
            holder.restaurantId = restaurant.id
            holder.nameTv.text = restaurant.name
            holder.distanceTv.text = "DISTANCE: ${restaurant.distance/1000} km"
            holder.addressTv.text = "Loading Address..." //set value after delay

            holder.setFavoriteIv.imageTintList = when(PreferencesHelper.checkFavId(restaurant.id)){
                false-> ColorStateList.valueOf(Color.LTGRAY)
                true-> ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
            }


            if (restaurant.distance < TOO_FAR_VAL){
                holder.setFavoriteIv.setOnClickListener{

                    when(PreferencesHelper.checkFavId(restaurant.id)){
                        false-> {
                            PreferencesHelper.addFavId(restaurant.id)
                            holder.setFavoriteIv.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorAccent))
                        }
                        true-> {
                            PreferencesHelper.removeFavId(restaurant.id)
                            holder.setFavoriteIv.imageTintList = ColorStateList.valueOf(Color.LTGRAY)

                            favoriteRemovedListener?.onFavoriteRemoved(position)
                        }
                    }

                }

                holder.setFavoriteIv.isEnabled = true

                holder.cardV.backgroundTintList = ColorStateList.valueOf(Color.WHITE)
            }else{
                holder.setFavoriteIv.isEnabled = false

                holder.cardV.backgroundTintList = ColorStateList.valueOf(Color.LTGRAY)
            }


            holder.featuresLl.removeAllViews()
            when(restaurant.features.size){
                0 -> holder.featuresLl.visibility = View.GONE

                else-> {
                    for(feature in restaurant.features){
                        holder.featuresLl.addView(TextView(context).apply {
                            this.text = feature
                        })
                    }
                    holder.featuresLl.visibility = View.VISIBLE
                }
            }


            //use sleep only because it is required by the test. otherwise Observable.timer is recommended
            Thread{
                Thread.sleep(2000)
                Observable.just(holder.restaurantId)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe{
                        restaurantId->
                        if (holder.restaurantId == restaurantId) {
                            holder.addressTv.text = "ADDRESS: ${restaurant.address}"
                        }
                    }
            }.start()


//            Observable.timer(2, TimeUnit.SECONDS)
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .subscribe(
//                    {restaurantId->
//                        if (holder.restaurantId == restaurantId) {
//                            holder.addressTv.text = restaurant.address
//                        }
//                    },
//
//                    {err->
//                        Log.e(TAG, err.localizedMessage)
//                    }
//                )

        }
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    private inner class RecyclerViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val cardV: CardView = mView.findViewById(R.id.cardV)
        val nameTv: TextView = mView.findViewById(R.id.name_tv)
        val addressTv: TextView = mView.findViewById(R.id.address_tv)
        val distanceTv: TextView = mView.findViewById(R.id.distance_tv)
        val setFavoriteIv: ImageView = mView.findViewById(R.id.set_favorite_iv)
        val featuresLl: LinearLayout = mView.findViewById(R.id.feature_ll)
        var restaurantId: Long? = null
    }

}

interface IFavoriteRemovedListener{
    fun onFavoriteRemoved(position: Int)
}