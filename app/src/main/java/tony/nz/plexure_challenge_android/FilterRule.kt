package tony.nz.plexure_challenge_android

import tony.nz.plexure_challenge_android.uitis.PreferencesHelper
import tony.nz.plexure_challenge_android.uitis.Restaurant

interface IRestaurantFilterRule{
    fun filter(item: Restaurant): Boolean
    val FILTER_NAME: String
}

class FavoriteRestaurantFilter: IRestaurantFilterRule{
    override val FILTER_NAME = "FeatureSelectedFilter"
    override fun filter(item: Restaurant): Boolean {
        return PreferencesHelper.checkFavId(item.id)
    }
}

class FeatureSelectedFilter(private val mSelectedFeatureSet: Set<String>): IRestaurantFilterRule{
    override val FILTER_NAME = "FeatureSelectedFilter"
    override fun filter(item: Restaurant): Boolean {
        for(feature in item.features){
            if (mSelectedFeatureSet.contains(feature)){
                return true
            }
        }
        return false
    }
}