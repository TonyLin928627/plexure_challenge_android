package tony.nz.plexure_challenge_android.uitis
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

object PreferencesHelper {
    private val TAG = "PreferencesHelper"

    private lateinit var sharedPreferences: SharedPreferences

    fun install(context: Context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun addFavId(favId: Long): Boolean{
        return sharedPreferences.edit().putBoolean(favId.toString(), true).commit()
    }

    fun removeFavId(favId: Long): Boolean{
        return sharedPreferences.edit().remove(favId.toString()).commit()
    }

    fun checkFavId(favId: Long): Boolean{
        return sharedPreferences.getBoolean(favId.toString(), false)
    }
}