package tony.nz.plexure_challenge_android.uitis

import android.util.Log
import com.google.gson.annotations.SerializedName
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.HttpURLConnection.HTTP_OK
import java.net.URL
import org.json.JSONArray
import tony.nz.plexure_challenge_android.DATA_URL


const val DataAPI_TAG = "DataAPI"

data class Restaurant(
    @SerializedName("id") val id: Long,
    @SerializedName("name") val name: String,
    @SerializedName("address") val address: String,
    @SerializedName("latitude") val latitude: Double,
    @SerializedName("longitude") val longitude: Double,
    @SerializedName("distance") val distance: Double,
    @SerializedName("featureList") val features: List<String>
)

object NetworkingHelper{
    fun loadData() : List<Restaurant>? {

        val rtnList = mutableListOf<Restaurant>()
        val stringBuffer = StringBuffer()
        val url = URL(DATA_URL)

        val connection = url.openConnection() as HttpURLConnection
        connection.requestMethod = "GET"
        connection.connect()
        if (connection.getResponseCode() === HTTP_OK) {
            val reader = BufferedReader(InputStreamReader(connection.getInputStream()))
            var line = reader.readLine()
            while (line != null) {
                Log.d(DataAPI_TAG, "Read $line")
                stringBuffer.append(line)
                line = reader.readLine()

            }
        }

        val jsonArray = JSONArray(stringBuffer.toString())
        Log.d(DataAPI_TAG, "Read ${jsonArray.length()} restaurant(s)")
        for(i in 0 until jsonArray.length()){
            val jsonObj = jsonArray.getJSONObject(i)

            val featureList = jsonObj.getJSONArray("featureList")
            val features = mutableListOf<String>()
            for(j in 0 until featureList.length()) {
                features.add(featureList.getString(j))
            }

            val restaurant = Restaurant(
                id = jsonObj.getLong("id"),
                name = jsonObj.getString("name"),
                address = jsonObj.getString("address"),
                latitude = jsonObj.getDouble("latitude"),
                longitude = jsonObj.getDouble("longitude"),
                distance = jsonObj.getDouble("distance"),
                features = features
            )

            rtnList.add(restaurant)
        }
        return rtnList

    }
}
